# ids721-week9-miniProj



## Getting started
1. install streamlit by running `pip install streamlit`
2. add dependencies into requirements.txt
3. Create streamlit_app.py
4. test locally by running `streamlit run streamlit_app.py` \
![](1.png)

5. Push to github  
6. Deploy the app using streamlit cloud
![](2.png)
7. test the LLM model
![](3.png) \ 
![](4.png)